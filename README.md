# sairemgmp20ked conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/sairemgmp20ked"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sairemgmp20ked module
